<?php

namespace Drupal\stoplight_elements_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Plugin implementation of the 'stoplight_elements_ui' formatter.
 *
 * @FieldFormatter(
 *   id = "stoplight_elements_ui",
 *   label = @Translation("Stoplight Elements UI"),
 *   description = @Translation("Formats file fields with Stoplight Elements YAML or JSON
 *   files with a rendered Stoplight Elements UI"), field_types = {
 *     "file"
 *   }
 * )
 */
class StoplightElementsUIFormatter extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $elements = parent::view($items, $langcode);
    $elements['#attached']['library'][] = 'stoplight_elements_field_formatter/stoplight_elements_field_formatter.stoplight';
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $stoplight_file = file_create_url($file->getFileUri());
      $element[$delta] = [
        '#theme' => 'stoplight_elements_ui_field_item',
        '#field_name' => $this->fieldDefinition->getName(),
        '#delta' => $delta,
        '#file_url' => $stoplight_file,
      ];
    }
    return $element;
  }

}
